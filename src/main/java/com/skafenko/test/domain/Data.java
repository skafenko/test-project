package com.skafenko.test.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.skafenko.test.json.MyHexInteger;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Entity
@Table(name = "data")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonDeserialize(using = MyHexInteger.class)
    private BigDecimal id;

    private String hash;
    private String format;
    private String url;
    private String title;
    private String documentOf;
    private OffsetDateTime datePublished;
    private OffsetDateTime dateModified;

    @JsonDeserialize(using = MyHexInteger.class)
    private BigDecimal relatedItem;

    public OffsetDateTime getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(OffsetDateTime datePublished) {
        this.datePublished = datePublished;
    }

    public OffsetDateTime getDateModified() {
        return dateModified;
    }

    public void setDateModified(OffsetDateTime dateModified) {
        this.dateModified = dateModified;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDocumentOf() {
        return documentOf;
    }

    public void setDocumentOf(String documentOf) {
        this.documentOf = documentOf;
    }

    public BigDecimal getRelatedItem() {
        return relatedItem;
    }

    public void setRelatedItem(BigDecimal relatedItem) {
        this.relatedItem = relatedItem;
    }
}
