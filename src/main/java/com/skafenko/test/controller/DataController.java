package com.skafenko.test.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.skafenko.test.json.JacksonObjectMapper;
import com.skafenko.test.domain.Data;
import com.skafenko.test.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class DataController {
    private DataService service;

    @Autowired
    public DataController(DataService service) {
        this.service = service;
    }

    @PostMapping(value = "/documents", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody String documents) {
        TypeReference<Map<String, List<Data>>> typeReference = new TypeReference<Map<String, List<Data>>>(){};
        try {
            Map<String, List<Data>> dataList  = JacksonObjectMapper.getMapper().readValue(documents, typeReference);
            service.save(dataList.get("data"));
            return ResponseEntity.ok(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}
