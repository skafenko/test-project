package com.skafenko.test.service;

import com.skafenko.test.domain.Data;
import com.skafenko.test.repository.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class DataService {
    private DataRepository repository;

    @Autowired
    public DataService(DataRepository repository) {
        this.repository = repository;
    }

    public void save(List<Data> dataList) {
        Objects.requireNonNull(dataList);
        repository.save(dataList);
    }
}
