package com.skafenko.test.repository;

import com.skafenko.test.domain.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Repository
public class DataRepository {

    private CrudRepository repository;

    public DataRepository(CrudRepository repository) {
        this.repository = repository;
    }

    public void save(List<Data> entities) {
        repository.save(entities);
    }
}
