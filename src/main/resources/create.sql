DROP TABLE IF EXISTS data;
CREATE TABLE data(
  id DECIMAL PRIMARY KEY NOT NULL,
  hash VARCHAR,
  format VARCHAR,
  url VARCHAR,
  title VARCHAR,
  document_of VARCHAR,
  date_published TIMESTAMP WITH TIME ZONE,
  date_modified TIMESTAMP WITH TIME ZONE,
  related_item DECIMAL
);
Delete FROM data WHERE id = 223763244764008129817077194668688971327;
INSERT INTO data (id, hash, format, url, title, document_of, date_published, date_modified, related_item)
VALUES
  (61240291815480407668589716637940426178, 'md5:70d30a1c3a7c7777', 'application/vnd.openxml'
    , '-sandbox.openpro', '-sandbox.openpro', 'tender', '2017-09-19 10:13:09.785229+03', '2017-09-19 10:13:09.785229+03', null);