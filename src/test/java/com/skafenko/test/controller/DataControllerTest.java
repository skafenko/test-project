package com.skafenko.test.controller;

import okhttp3.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class DataControllerTest {

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public static final String URL =
            "https://lb.api-sandbox.openprocurement.org/api/2.4/contracts/800384f02be04ce091277202897de676/documents";
    public static final String LOCALHOST = "http://LOCALHOST:8080/documents";
    @Test
    public void saveOK() throws Exception {
        OkHttpClient client = new OkHttpClient();

        Request jsonRequest = new Request.Builder()
                .url(URL)
                .build();
        Response responses = client.newCall(jsonRequest).execute();
        String jsonData = responses.body().string();

        RequestBody body = RequestBody.create(JSON, jsonData);
        Request request = new Request.Builder()
                .url(LOCALHOST)
                .post(body)
                .build();
        Response restResponse = client.newCall(request).execute();
        assertEquals(restResponse.code(), 200);
    }

    @Test
    public void saveEmpty() throws Exception {
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, "");
        Request request = new Request.Builder()
                .url(LOCALHOST)
                .post(body)
                .build();
        Response restResponse = client.newCall(request).execute();
        assertEquals(restResponse.code(), 400);
    }
}