Необходимо создать мини проект, который будет загружать JSON данные из HTTP REST сервера
в БД.
+ Необходимо написать пару тестов, которые проверяют работу функционала.
+ проект выложить на Git и выслать ссылку
1. Весть проект на SpringBoot, любой версии.
2. Сборка Maven
3. http клиент на ваш выбор, предпочтительно OkHttp
4. БД на ваш выбор, предпочтительно postgres
5. Тест WireMock или JUnit

https://lb.api-sandbox.openprocurement.org/api/2.4/contracts/ffb2e977797440719208b510ed91548b/documents

https://lb.api-sandbox.openprocurement.org/api/2.4/contracts/6aa21ab58b0f402591ef4cc6abebe6c3/documents

https://lb.api-sandbox.openprocurement.org/api/2.4/contracts/800384f02be04ce091277202897de676/documents